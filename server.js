'use strict';

const express = require('express'),
    bodyParser = require('body-parser'),
    path = require('path'),
    wine = require('./routes/wines.js');

// Express
const app = express();

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());


const MongoClient = require('mongodb').MongoClient;
// Database url
const url = 'mongodb://localhost:27017/WINES';

// Connect to the database.
MongoClient.connect(url, function(err, database) {
    if (err){
        throw err;
    }
    // Connected to db
    console.log(`Connected to ${url} database`);
    // Initialize the app
    app.listen(3000);
    console.log('Listening on port 3000...!');
    // Make global db reference for rest of app to make req's to database
    global.db = database;
});


// Wines API routes

app.get('/', function (req, res){
    res.sendFile(path.join(__dirname+'/index.html'));
});
app.get('/wines', wine.findAll);
app.get('/wines/:id', wine.findById);
app.post('/wines', wine.addWine);
app.put('/wines/:id', wine.updateWine);
app.delete('/wines/:id', wine.deleteWine);
