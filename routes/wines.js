'use strict';
const mongo = require('mongodb');
const ObjectID = mongo.ObjectID;


/**
 * handleError
 * @param  {[type]} res
 * @param  {[type]} reason
 * @param  {[type]} message
 * @param  {[type]} code
 * @return {null}
 */
function handleError(res, reason, message, code) {
	console.log('ERROR: ' + reason);
	res.status(code || 500).json({
		'error': message
	});
}

/**
 * findAll wines stored in database
 * @param  {[type]} req
 * @param  {[type]} res
 * @return {null}  return list
 */
exports.findAll = function (req, res) {
	db.collection('types').find({}).toArray(function (err, items) {
		if (err) {
			handleError(res, err.message, 'Failed to get all wines');
		} else {
			res.send(JSON.stringify(items, null, 2));
		}
	});
};

/**
 * find wine by ID in database
 * @param  {[type]} req
 * @param  {[type]} res
 * @return {null}
 */
exports.findById = function (req, res) {
	const id = req.params.id;
	console.log('Retrieving wine: ' + id);

	db.collection('types').findOne({
			'_id': new ObjectID(id)
		},
		function (err, item) {
			if (err) {
				handleError(res, err.message, 'Failed to get wine by ID');
			} else {
				res.send(item);
			}
		}
	)
};

/**
 * addWine
 * @param {[type]} req
 * @param {[type]} res
 */
exports.addWine = function (req, res) {
	const wine = req.body;
	db.collection('types').insert(
		wine,
		function (err, result) {
			if (err) {
				handleError(res, err.message, 'Failed to add new wine.');
			} else {
				console.log('Success: \n' + JSON.stringify(result.ops[0]));
				res.send(result.ops[0]);
			}
		}
	)
};

/**
 * update wine in database
 * @param  {[type]} req
 * @param  {[type]} res
 * @return {null}
 */
exports.updateWine = function (req, res) {
	const id = req.params.id;
	const wine = req.body;

	console.log('Updating wine: ' + id);
	console.log(JSON.stringify(wine));

	db.collection('types').update({
			'_id': new ObjectID(id)
		},
		wine,
		function (err, result) {
			if (err) {
				handleError(res, err.message, 'Failed to update wine.');
			} else {
				console.log('' + result + ' document(s) updated');
				res.send(wine);
			}
		}
	)
};

/**
 * delete wine from database
 * @param  {[type]} req
 * @param  {[type]} res
 * @return {null}
 */
exports.deleteWine = function (req, res) {
	const id = req.params.id;
	console.log('Deleting wine: ' + id);

	db.collection('types').remove({
			'_id': new ObjectID(id)
		},
		function (err, item) {
			if (err) {
				handleError(res, err.message, 'Failed to get wine by ID');
			} else {
				res.send(item);
			}
		}
	)
};
