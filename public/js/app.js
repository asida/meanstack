angular.module('wineApp', ['ngRoute','ngMaterial'])
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'list.html',
                controller: 'ListController',
                resolve: {
                    wines: function( Wines ) {
                        return Wines.getWines();
                    }
                }
            })
            .when('/new/wine', {
                controller: 'NewWineController',
                templateUrl: 'new-wine-form.html'
            })
            .when('/edit/wine/:wineId', {
                controller: 'EditWineController',
                templateUrl: 'edit-wine-form.html'
            })
            .otherwise({
                redirectTo: '/'
            })
    })
    .service('Wines', function($http) {
        this.getWines = function() {
            return $http.get('/wines').
                then(function( response ) {
                    return response;
                }, function(response) {
                    alert('Error finding wines.');
                });
        }
        this.createWine = function( wine ) {
            return $http.post('/wines', wine).
                then(function( response ) {
                    return response;
                }, function( response ) {
                    alert( 'Error creating wine.' );
                });
        }
        this.getOneWine = function( wineId ) {
            const url = '/wines/' + wineId;
            return $http.get( url ).
                then(function( response ) {
                    return response;
                }, function( response ) {
                    alert( 'Error finding this wine.' );
                });
        }
        this.updateWine = function( data, wineId ) {
            const map = data;
            delete map._id;

            const url = '/wines/' + wineId;
            return $http.put( url, map, wineId ).
                then(function( response ) {
                    console.log(map)
                    return response;
                }, function( response ) {
                    alert( 'Error editing this wine.' ,data);
                    console.log(response);
                });
        }
        this.deleteWine = function( wineId ) {
            const url = '/wines/' + wineId;
            return $http.delete( url ).
                then(function( response ) {
                    return response;
                }, function( response ) {
                    alert( 'Error deleting this wine.' );
                    console.log( response );
                });
        }
    })

    /**
     * Controllers logic
     */
    .controller('ListController', function(wines, $scope) {
        $scope.wines = wines.data;
    })

    .controller('NewWineController', function($scope, $location, Wines) {
        $scope.back = function() {
            $location.path('#/');
        }

        $scope.saveWine = function( wine ) {
            if(wine == undefined){
                $location.path('#/');
                return;
            }
            Wines.createWine( wine ).then(function( doc ) {
                const wineUrl = '/wine/' + doc.data._id;
                $location.path( wineUrl );
            }, function( response ) {
                alert( response );
            });
        }
    })

    .controller('EditWineController', function($scope, $location, $routeParams, Wines) {
        Wines.getOneWine($routeParams.wineId).then(function( doc ) {
            $scope.wine = doc.data;
        }, function(response) {
            alert(response);
        });

        $scope.saveEditedWine = function( data, wineId ) {
            Wines.updateWine( data, wineId ).then(function( doc ) {
                const wineUrl = '/wine/' + doc.data._id;
                $location.path( wineUrl );
            }, function( response ) {
                alert( response );
            });
        }

        $scope.deleteWine = function( wineId ) {
            Wines.deleteWine( wineId ).then(function( doc ) {
                const wineUrl = '/wine/' + doc.data._id;
                $location.path( wineUrl );
            }, function( response ) {
                alert( response );
            });
        }
    });
